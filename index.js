var rp = require("request-promise");
const moment = require("moment");
const cron = require("node-cron");
const { sendMail } = require("./node-mailer");
const recievers = require("./recievers");

cron.schedule("0 */1 * * * *", () => {
  console.log("running a task every hour");
  findSlots();
});



const findSlots = async () => {  
  let dates = [
    moment().format("DD-MM-YYYY"),
    moment().add(1, "days").format("DD-MM-YYYY"),
    moment().add(2, "days").format("DD-MM-YYYY"),
    moment().add(3, "days").format("DD-MM-YYYY"),
    moment().add(4, "days").format("DD-MM-YYYY"),
  ];

  for (let set of recievers) {
    let availableFreeCenters = "";
    let availablePaidCenters = "";
    for (let currentDate of dates) {
      for (let district of set.districtIDs) {
        let requestUrl = `https://cdn-api.co-vin.in/api/v2/appointment/sessions/calendarByDistrict?district_id=${district}&date=${currentDate}`;
        try {
          let data = await rp(requestUrl);
          for (let center of JSON.parse(data).centers) {
            for (let session of center.sessions) {
              if (session.min_age_limit == 18 && session.available_capacity > 3) {
                if (center.fee_type == "Free") {
                  availableFreeCenters += `<div> ${currentDate} - ${center.name}(${center.pincode}) - ${session.available_capacity}</div>`;
                } else {
                  for (let fee of center.vaccine_fees) {
                    availablePaidCenters += `<div> ${currentDate} - ${center.name}(${center.pincode}) - ${session.available_capacity} - ${fee.vaccine} - ${fee.fee}</div>`;
                  }
                }
              }
            }
          }
        } catch (err) {}
      }
    }
    let availableCenters = "";
    if (availableFreeCenters) {
      availableCenters += `<div style="font-weight:bold;"> Free Centers </div>`;
      availableCenters += availableFreeCenters;
    }
    if (availablePaidCenters) {
      availableCenters += `<div style="font-weight:bold;"> Paid Centers </div>`;
      availableCenters += availablePaidCenters;
    }    
    if (availableCenters.length) {   
      for(let mailID of set.mailIds.split(", ")){
        sendMail({
          to: mailID,
          subject: "COVID Vaccine slots",
          html: availableCenters,
        });
      }
      
    }
  }

  console.log("DOne");
};

