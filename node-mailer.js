require('dotenv').config()
const nodemailer = require("nodemailer");

let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
      user: process.env.user, // generated ethereal user
      pass: process.env.pass, // generated ethereal password
    },
  });

const sendMail = async (data) => {    

    const msg = {
        to: data.to,
        from: process.env.user,
        subject: data.subject,        
        html: data.html,
      };
      try{
        let info = await transporter.sendMail(msg);
        console.log(info);
      }catch(err){
        console.log(err);
      }
      
      
}

module.exports = {
    sendMail
}

